# site-grepper
a generic utility for finding instances of a word on a website (or list of websites), returns full HTML element containing match

automatically crawls to any found URLs sharing base path with given path

## usage
- clone repository
- instantiate deps with ```pipenv shell```
- create file named 'url_list' with newline-delimited list of URLs to crawl
- run with ```./grepper.py <string-to-find> <OPTIONAL:single-url-base>
